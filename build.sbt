import Dependencies._

ThisBuild / scalaVersion := "2.13.0"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

val akka = "2.5.25"
val akkaHttp = "10.1.9"

lazy val root = (project in file("."))
  .settings(
    name := "akka-game",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttp,
      "com.typesafe.akka" %% "akka-stream" % akka,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttp,
      "com.typesafe.akka" %% "akka-stream-testkit" % akka % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttp % Test,
      scalaTest % Test,
    )
  )

lazy val client = (project in file("client"))
  .settings(
    name := "akka-client",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akka,
      "com.typesafe.akka" %% "akka-stream" % akka,
      "com.typesafe.akka" %% "akka-http" % akkaHttp,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttp,
      "org.scalafx" %% "scalafx" % "12.0.2-R18",
      "com.typesafe.akka" %% "akka-stream-testkit" % akka % Test,
      scalaTest % Test,
    ),
  )
