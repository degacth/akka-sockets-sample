package example

import akka.actor.PoisonPill
import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import org.scalatest.{BeforeAndAfterEach, Matchers, WordSpecLike}

class ServerSpec extends WordSpecLike with Matchers with ScalatestRouteTest with BeforeAndAfterEach {

  var server: GameServer = _

  override def beforeEach(): Unit = {
    server = new GameServer(system)
  }

  override def afterEach(): Unit = {
    system.actorSelection("/user/*") ! PoisonPill
  }

  "Game Server" must {
    val john = "John"
    val welcomeJohnMessage = s"""[{"name":"$john","position":{"x":0,"y":0}}]"""

    "should be able to connect to server" in {
      assertClient(john) { _ =>
        isWebSocketUpgrade shouldEqual true
      }
    }

    "should respond with correct message" ignore {
      assertClient(john) { wsClient =>
        wsClient expectMessage welcomeJohnMessage
        wsClient sendMessage "hello"
        wsClient expectMessage "hello"
      }
    }

    "should register player" in {
      assertClient(john) { wsClient =>
        wsClient expectMessage welcomeJohnMessage
      }
    }

    "should register multiple players" in {
      assertClient(john) { wsClient =>
        wsClient expectMessage s"""[{"name":"$john","position":{"x":0,"y":0}}]"""
      }

      val dave = "Dave"
      assertClient(dave) { wsClient =>
        wsClient expectMessage s"""[{"name":"$john","position":{"x":0,"y":0}},{"name":"$dave","position":{"x":0,"y":0}}]"""
      }
    }

    "should register player and move it up" in {
      assertClient(john) { wsClient =>
        wsClient expectMessage s"""[{"name":"$john","position":{"x":0,"y":0}}]"""
        wsClient sendMessage "up"
        wsClient expectMessage s"""[{"name":"$john","position":{"x":0,"y":1}}]"""
      }
    }
  }

  private def assertClient(playerName: String)(assertions: WSProbe => Unit): Unit = {
    val wsClient = WSProbe()
    WS(server.wsUrl(playerName), wsClient.flow) ~> server.webSocketRoute ~> check {
      assertions(wsClient)
    }
  }
}

