package example

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.scaladsl.{Flow, GraphDSL, Merge, Sink, Source}
import akka.stream.{FlowShape, OverflowStrategy}

class GameServer(system: ActorSystem) extends Directives {
  val webSocketRoute: Route = (get & parameter("playerName")) { playerName =>
    handleWebSocketMessages(flow(playerName))
  }

  val gameArea: ActorRef = system.actorOf(Props(new GameArea), "game-area")
  private val playerActor = Source.actorRef[GameArea.GameEvent](5, OverflowStrategy.fail)

  def wsUrl(name: String) = s"/?playerName=$name"

  def flow(playerName: String): Flow[Message, Message, Any] =
    Flow.fromGraph(GraphDSL.create(playerActor) { implicit builder =>
      playerActor =>
        import GraphDSL.Implicits._

        println(s"connected new player: $playerName")

        val materialization = builder.materializedValue.map {
          actor => GameArea.PlayerJoined(GameArea.Player(playerName, GameArea.Position(0, 0)), actor)
        }
        val merge = builder.add(Merge[GameArea.GameEvent](2))

        val messagesToGameEventsFlow = builder.add(Flow[Message] collect {
          case TextMessage.Strict(direction) =>
            GameArea.PlayerMove(playerName, direction)
        })

        val gameEventsToMessagesFlow = builder.add(Flow[GameArea.GameEvent] collect {
          case GameArea.PlayersChanged(players) =>
            import spray.json._
            import DefaultJsonProtocol._

            implicit val positionFormat: RootJsonFormat[GameArea.Position] =
              jsonFormat2(GameArea.Position)

            implicit val playerFormat: RootJsonFormat[GameArea.Player] =
              jsonFormat2(GameArea.Player)
            TextMessage(players.toJson.toString)
        })

        val gameAreaSink = Sink.actorRef[GameArea.GameEvent](gameArea, GameArea.PlayerLeft(playerName))

        materialization ~> merge ~> gameAreaSink
        messagesToGameEventsFlow ~> merge
        playerActor ~> gameEventsToMessagesFlow

        FlowShape(messagesToGameEventsFlow.in, gameEventsToMessagesFlow.out)
    })
}
