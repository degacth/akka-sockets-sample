package example

import akka.actor.{Actor, ActorRef}

class GameArea extends Actor {

  import GameArea._

  private val players = collection.mutable.LinkedHashMap.empty[String, PlayerWithActor]

  override def receive: Receive = {
    case PlayerJoined(player, actor) =>
      players += player.name -> PlayerWithActor(player, actor)
      notifyPlayersChanged()

    case PlayerLeft(playerName) =>
      players -= playerName
      notifyPlayersChanged()

    case PlayerMove(playerName, direction) =>
      val offset = direction match {
        case "up" => Position(0, 1)
        case "down" => Position(0, -1)
        case "right" => Position(1, 0)
        case "left" => Position(-1, 0)
      }

      val currentPlayerWithActor = players(playerName)
      val currentPlayer = currentPlayerWithActor.player
      players(playerName) = currentPlayerWithActor.copy(
        player = currentPlayer.copy(position = currentPlayer.position + offset)
      )

      notifyPlayersChanged()
  }

  private def notifyPlayersChanged(): Unit = {
    val playersChanged = players.values.map(_.player)

    players.values foreach { player =>
      player.actor ! PlayersChanged(playersChanged)
    }
  }
}

object GameArea {

  sealed trait GameEvent

  case class PlayerJoined(player: Player, actorRef: ActorRef) extends GameEvent

  case class PlayerLeft(playerName: String) extends GameEvent

  case class PlayerMove(playerName: String, direction: String) extends GameEvent

  case class PlayersChanged(players: Iterable[Player]) extends GameEvent


  case class Player(name: String, position: Position)

  case class PlayerWithActor(player: Player, actor: ActorRef)

  case class Position(x: Int, y: Int) {
    def +(other: Position) = Position(x + other.x, y + other.y)
  }

}
