package example

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import scala.io.StdIn

object Main extends App {
  implicit val system = ActorSystem("app")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val server = new GameServer(system)

  val binding = Http().bindAndHandle(server.webSocketRoute, "localhost", 8080)
  StdIn.readLine()

  binding flatMap (_.unbind()) onComplete (_ => system.terminate())
}
