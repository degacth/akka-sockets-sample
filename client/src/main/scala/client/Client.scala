package client

import akka.actor._
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{TextMessage, WebSocketRequest}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Keep, Sink, Source}
import scalafx.application.{JFXApp, Platform}
import scalafx.scene.Scene
import scalafx.scene.input.{KeyCode, KeyEvent}
import scalafx.scene.layout.{AnchorPane, StackPane}
import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle
import scalafx.scene.text.Text
import spray.json.DefaultJsonProtocol

import scala.io.StdIn

object Main extends App {
  implicit val system = ActorSystem("app")
  implicit val mat = ActorMaterializer()

  val name = StdIn.readLine()

  val client = new Client(name)

  val display = new Display()
  val input = Source.actorRef[String](5, OverflowStrategy.dropNew)
  val output = display.sink
  val ((inputMat: ActorRef, result), outputMat) = client.run(input, output)
  val keyBoardHandler = new KeyBoardHandler(inputMat)

  new GUI(keyBoardHandler, display).main(args)
}

class GUI(handler: KeyBoardHandler, display: Display) extends JFXApp {

  import scalafx.Includes._

  stage = new JFXApp.PrimaryStage {
    title.value = "client"
    scene = new Scene {
      content = display.panel
      onKeyPressed = (ev: KeyEvent) => handler.handle(ev)
    }
  }
}

class Client(name: String)(implicit val system: ActorSystem, val mat: ActorMaterializer) extends DefaultJsonProtocol {

  import spray.json._

  implicit val positionFormat = jsonFormat2(Position)
  implicit val playerFormat = jsonFormat2(Player)

  val webSocketFlow = Http().webSocketClientFlow(
    WebSocketRequest(s"ws://localhost:8080/?playerName=$name")
  ) collect {
    case TextMessage.Strict(strMsg) => strMsg.parseJson.convertTo[List[Player]]
  }

  def run[M1, M2](input: Source[String, M1], output: Sink[List[Player], M2]) = {
    input.map { direction => TextMessage(direction) }
      .viaMat(webSocketFlow)(Keep.both)
      .toMat(output)(Keep.both)
      .run()
  }
}

case class Player(name: String, position: Position)

case class Position(x: Int, y: Int)

class KeyBoardHandler(keyboardEventsReceiver: ActorRef) {
  def handle(keyEvent: KeyEvent) = keyEvent.code match {
    case KeyCode.Up => keyboardEventsReceiver ! "up"
    case KeyCode.Down => keyboardEventsReceiver ! "down"
    case KeyCode.Left => keyboardEventsReceiver ! "left"
    case KeyCode.Right => keyboardEventsReceiver ! "right"
  }
}

class Display {
  private val PlayerRadius = 100
  private val Dimensions = 6
  private val ScreenSize = PlayerRadius * Dimensions

  val panel = new AnchorPane {
    minWidth = ScreenSize
    minHeight = ScreenSize
  }

  def sink = Sink.foreach[List[Player]] { playerPositions =>
    val playerShapes = playerPositions.map { player =>
      new StackPane {
        minWidth = ScreenSize
        minHeight = ScreenSize
        layoutX = player.position.x * PlayerRadius
        layoutY = player.position.y * PlayerRadius
        prefHeight = PlayerRadius
        prefWidth = PlayerRadius

        val circlePlayer = new Circle {
          radius = PlayerRadius * 0.5
          fill = getColorForPlayer(player.name)
        }

        val textOnCircle = new Text {
          text = player.name
        }

        children = Seq(circlePlayer, textOnCircle)

        def getColorForPlayer(name: String) = {
          val r = 55 + math.abs(("r" + name).hashCode) % 200
          val g = 55 + math.abs(("g" + name).hashCode) % 200
          val b = 55 + math.abs(("b" + name).hashCode) % 200
          Color.rgb(r, g, b)
        }
      }
    }

    Platform.runLater({
      panel.children = playerShapes
      panel.requestLayout()
    })
  }
}
