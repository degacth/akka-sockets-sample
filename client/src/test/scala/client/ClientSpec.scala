package client

import akka.actor.ActorSystem
import akka.http.scaladsl.model.ws.WebSocketRequest
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import org.scalatest.{Matchers, WordSpecLike}

class ClientSpec extends WordSpecLike with Matchers {
  implicit val system = ActorSystem("test-app")
  implicit val mat = ActorMaterializer()

  val playerName = "Joe"

  "Game client" must {
    "able to login player" in {
      val expectedMsg = TextMessage.Strict(s"""[{"name":"$playerName","position":{"x":0,"y":0}}]""")
      val testSink = TestSink.probe[Message]
      val outgoing = Source.empty[Message]
      val webSocketFlow = Http().webSocketClientFlow(
        WebSocketRequest(s"ws://localhost:8080/?playerName=$playerName")
      )
      val (_, testProb) = outgoing
        .viaMat(webSocketFlow)(Keep.right)
        .toMat(testSink)(Keep.both)
        .run()

      testProb request 1
      testProb.expectNext(expectedMsg)
    }

    "be able to move player" in {
      val input = Source.actorRef[String](5, OverflowStrategy.dropNew)
      val output = TestSink.probe[List[Player]]
      val client = new Client(playerName)
      val ((inputMat, result), outputMat) = client.run(input, output)

      inputMat ! "up"

      outputMat.request(2)
      outputMat.expectNext(List(Player(playerName, Position(0, 0))))
      outputMat.expectNext(List(Player(playerName, Position(0, 1))))
    }
  }
}

